import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class ExampleServiceTest {

    @Test
    void sum() {
        ExampleService service = new ExampleService();
        assertEquals(4, service.sum(2, 2));
    }

    @Test
    void divide() {
        ExampleService service = new ExampleService();
        assertEquals(2, service.divide(4, 2));
    }

    @Test
    void divideByZeroShouldRaiseException() {
        ExampleService service = new ExampleService();
        assertThrows( ArithmeticException.class, () -> service.divide(4, 0));
    }
}