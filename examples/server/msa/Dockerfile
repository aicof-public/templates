FROM amazoncorretto:11-alpine3.13-jdk as builder
WORKDIR application
COPY target/*.jar /application/application.jar
RUN java -Djarmode=layertools -jar application.jar extract
RUN jdeps -cp '/application/dependencies/BOOT-INF/lib/*' -recursive --multi-release 11 --print-module-deps --ignore-missing-deps /application/application.jar > /application/jre-deps.info

RUN jlink --verbose \
  --compress 2 \
  --strip-debug \
  --no-header-files \
  --no-man-pages \
  --output /application/jre \
  --add-modules $(cat jre-deps.info)

FROM alpine:latest
WORKDIR application

EXPOSE $PORT $SECPORT

COPY --from=builder application/jre jre
COPY --from=builder application/dependencies/ /application
COPY --from=builder application/spring-boot-loader /application
COPY --from=builder application/snapshot-dependencies/ /application
COPY --from=builder application/application/ /application

RUN addgroup -S sbuser && adduser -S sbuser -G sbuser
USER sbuser
ENTRYPOINT ["/application/jre/bin/java", "org.springframework.boot.loader.JarLauncher", "-Xnoverify", "-Dspring-boot.run.arguments=--spring.jmx.enabled=false,spring.main.lazy-initialization=true" ]